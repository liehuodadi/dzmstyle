# discuz X3.4自适应手机模板

#### 项目介绍
discuz X3.4默认模板的手机模板，在系统默认模板的基础上开发的和电脑端一样的手机版模板，完全自适应方式，使用最新版bootstrap4.1.3前端框架制作，，熟悉bootstrap框架的人也就可以快速开始咯。

#### 关于模板
discuz程序版本为discuz! X3.4最新版，UTF-8编码（需要其他编码的小伙伴请自行转码哒），使用bootstrap4.1.3前端框架，仅手机端模板（触屏版），支持和默认的电脑模板相同的展示效果和功能。支持所有电脑版全部功能显示、发帖、回帖、交易等，支持分类信息，电脑上怎么样的手机上也一样，史无前例的牛逼哒。欢迎下载测试。


#### 安装教程

1. 全部打包下载所有文件。
2. 网站后台：全局---站点功能---其他---启用登录自动选择帐号:选择“是”。
3. 网站后台：全局---开启手机版:（是）---启用新触屏版:（否）---开启手机浏览器自动跳转:（是）；其他设置随便您。
4. 网站后台：界面---编辑器设置---编辑栏样式:选择“高级”。
8. 在您的网站根目录 template文件夹里面新建mobilestyle文件夹（文件夹名称只能是mobilestyle，不能用其他，原因是在调用css和js文件的路径用的是这个），将下载的全部文件拷贝到mobilestyle文件夹里面，然后进入网站后台：界面---风格管理---安装---安装完了以后要将“手机触屏版”选择为新安装的模板，下图所示
![输入图片说明](https://images.gitee.com/uploads/images/2018/0912/223850_4a3d981d_1211109.jpeg "搜狗截图20180912223742.jpg")

#### 使用说明
手机模板的文件全部在mobilestyle/touch文件夹里面。其他的不是手机模板文件哦。


#### 码云特技

